---
home: true
# heroImage: /hero.png
actionText: Hi →
actionLink: /
features:
- title: Test
  details: This is a test
- title: Test
  details: This is a test
- title: Test
  details: This is a test
footer: MIT Licensed | Copyright © 2018-present Nathanael Farley
---