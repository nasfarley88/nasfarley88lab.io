# Writing sample (LHCbPR)

## Preamble

During my PhD I worked on data from the
[LHC](https://en.wikipedia.org/wiki/Large_Hadron_Collider), specifically from
the [LHCb](http://lhcb-public.web.cern.ch/lhcb-public/) experiment. In the
course of maintaining the hardware and performing physics analyses, it is
essential to create simulations of the experiment based on theoretical
predictions to understand the data that we receive from it. We do this using a
custom simulation framework ([Gauss](http://lhcbdoc.web.cern.ch/lhcbdoc/gauss/))
which utilises many other projects such as
[Geant4](http://www.geant4.org/geant4/),
[PYTHIA](http://home.thep.lu.se/~torbjorn/Pythia.html),
[GaudiPython](https://twiki.cern.ch/twiki/bin/view/LHCb/GaudiPython) and others.

In software projects, testing is essential to build confidence that changes in
code have not adversely changed the output of the code. As part of the
testing suite for Gauss, integration tests are performed and the data produced
are examined by a loosely formed QA team (usually experts in parts
of the experiment that tests are designed to simulate).

LHCbPR2 is a full stack solution designed to run and display all of the tests
that can be run in Gauss, built primarily using a microservice architecture.
LHCbPR2FE is the web front-end designed to show the results of the integration
tests. These are usually displayed in the form of graphs and histograms.


At the end of 2016, my colleagues and I were introduced to the idea of LHCbPR2FE
to display the ongoing results of the integration tests we were managing. It
became clear to me that the current (at the time) way of working could be
greatly improved. Therefore, I proposed the following solution to my supervisor
(verbally with a whiteboard). This proposal lead to me going to CERN to work
with others on LHCbPR2 for a week to jump-start the process and then developing
TurboViews to the point where they were used by ~80 people, until the front end
was rewritten. TurboViews were then used as a template for the new front end.
Although not originally planned, this became a significant contribution to my
thesis.

The proposal below has been written for the purposes of 
providing a writing sample.

## Proposal for TurboViews in LHCbPR2

### Current state

Currently, physicists responsible for managing tests for Gauss (test authors)
are required to register an *analysis module* with LHCbPR2FE in order to display
the results of their test. This involves coding HTML, CSS and JavaScript. 

In general, test authors are familiar with Python and C++ but often lack
competency in web technologies like JavaScript and CSS. This leads test authors
spending significant time on displaying the results in the browser with an
unfamiliar JavaScript framework (AngularJS) and programming language(s) which
are not used elsewhere in their work. This can lead to poor code quality and
frustration, compounded by tests changing test authors every ~2 years.

In addition to this, as we move towards updating our dependency on Geant4 9.x to 
Geant4 10.x, more and more tests are
being integrated into the LHCbPR2 framework. The additional work associated with
each new test may slow down the adoption of Geant4 10.x as test authors work on
displaying their tests.

As most of the data test authors display in LHCbPR2FE is quite similar in
nature, I believe that with small changes to the code base, we can eliminate most
of this frustration and improve efficiency.

### Proposal

To address this problem, I propose that we create a single function that takes a
configuration object and creates an analysis module on behalf of the test
author. This will remove the need for test authors to learn JavaScript since all
code will be written inside a member function of the `Module` class and all
customisation will be done inside the configuration object. An analysis module
created in this way will be called a TurboView.

```javascript
myModule.registerTurboView({
  name: "TestEM5",
  description:
    "Test of the electromagnetic calorimeter with various physics lists",
  plotViews: ["splitView", "superImposedView", "differenceView"],
  defaultPlotView: "splitView",
  defaultPlots: [
    // References to plots produced by the test
  ],
  // Restrict which test will be shown, in this case a 
  // test of Gauss called GAUSS-EMPHYSICS
  restrict: {
    selectedApp: "GAUSS",
    selectedOptions: "GAUSS-EMPHYSICS"
  }
});
```

Implementing TurboViews using a factory function rather than rewriting the
underlying code for analysis modules means that existing tests will be
unaffected by the change. It also means that the developer time needed to
implement this feature will be much shorter and can be done by a single person.

As a stop-gap measure, a generic TurboView for all tests will be created, i.e.
it will be possible to look at the results of all compatible tests within a
single view. When test authors are satisfied that their tests will be displayed
properly within a TurboView, they can register their test using
`Module.prototype.registerTurboView` (as above) and provide defaults to be shown
when the view loads. This means that those responsible for checking the tests
can quickly see what is important.


#### Advantages

This approach has several advantages:

- Test authors only need to specify a config in a familiar format (JSON) to have
  their work displayed appropriately, saving them significant programming time
  and debugging a language they are unfamiliar with
- The code can be shared between different TurboViews, meaning that
  upgrades to UI are immediately applied across all tests using the
  TurboView functionality
- The TurboView code can be maintained by a small team of experts rather than by a
  large team of rapidly changing test authors

#### Weaknesses

This approach will not be for all tests however as:

- Since it is a generic tool, bespoke data representation may not be possible
  (e.g. displaying data in custom tables, custom fits to data in the browser)
- As a generic tool, it will need to be maintained by people who know
  JavaScript, HTML and CSS

### Conclusion

TurboViews will provide a way for test authors to have their tests displayed in 
the
browser without the work of writing their own JavaScript/HTML/CSS. 
Without a similar system, it may take test authors a
significant amount of time to learn JavaScript enough to make their own analysis
module (an estimated 2-4 weeks each, with around 10-20 tests that currently need
maintaining).

It will take me around 2 weeks to get a working prototype, and between 4-6 weeks
to get a fully working product. Since I already know JavaScript, I do not have
the additional overhead of learning a new language.